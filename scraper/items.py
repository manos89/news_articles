import scrapy
from scrapy.loader.processors import TakeFirst


class NewsItem(scrapy.Item):
    title = scrapy.Field(output_processor=TakeFirst())
    body = scrapy.Field(output_processor=TakeFirst())
    day = scrapy.Field(output_processor=TakeFirst())
    month = scrapy.Field(output_processor=TakeFirst())

