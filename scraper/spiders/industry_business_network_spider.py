import scrapy
from scraper.items import NewsItem


class project_scraperSpider(scrapy.Spider):
    name = "industry_business_network"
    start_urls = ["https://www.industry-business-network.org/de/news/", "https://www.industry-business-network.org/en/news/"]


    def parse(self, response):
        links = response.selector.css("div.mkdf-bli-content > h4.entry-title > a::attr(href)").extract()
        for link in links:
            yield scrapy.Request(link, callback=self.parse_article_details)

    def parse_article_details(self, response):
        item = NewsItem()
        item["title"] = response.selector.css('meta[property="og:title"]::attr(content)').extract_first()
        item["body"] = "".join(response.selector.css("div.mkdf-post-text-main > p::text").extract())
        item["day"] = response.selector.css("div.mkdf-post-info-date-day::text").extract_first()
        item["month"] = response.selector.css("div.mkdf-post-info-date-month::text").extract_first()
        yield item
